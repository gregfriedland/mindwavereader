import warnings
warnings.filterwarnings("ignore", message="numpy.dtype size changed")
warnings.filterwarnings("ignore", message="numpy.ufunc size changed")

import numpy as np
import pandas
import sys, os
import argparse
from mne.fiff.edf import read_raw_edf
from scipy import signal
import matplotlib.pyplot as plt
import mne
from mne.time_frequency import ar_raw
from mne.datasets import sample

mne.verbose=10

def ar_whiten(raw):
    order = 5
    
    # Estimate AR models on raw data
    coefs = ar_raw(raw, order=order, picks=[0])
    print "AR Coefficients:", coefs
    mean_coefs = np.mean(coefs, axis=0)  # mean model across channels

    filt = np.r_[1, -mean_coefs]  # filter coefficient
    d, times = raw[0,1024:]  # look at one channel from now on
    d = d.ravel()  # make flat vector
    innovation = signal.convolve(d, filt, 'valid')
    d_ = signal.lfilter([1], filt, innovation)  # regenerate the signal
    d_ = np.r_[d_[0] * np.ones(order), d_]  # dummy samples to keep signal length

    ###############################################################################
    # Plot the different time series and PSDs
    plt.close('all')
    plt.figure()
    plt.plot(d[:100], label='signal')
    plt.plot(d_[:100], label='regenerated signal')
    plt.legend()

    plt.figure()
    plt.psd(d, Fs=raw.info['sfreq'], NFFT=2048)
    plt.psd(innovation, Fs=raw.info['sfreq'], NFFT=2048)
    plt.psd(d_, Fs=raw.info['sfreq'], NFFT=2048, linestyle='--')
    plt.legend(('Signal', 'Innovation', 'Regenerated signal'))
    plt.show()

    
def plot_psd(raw):
    #psd, freqs = mne.time_frequency.compute_raw_psd(raw=raw, picks=[0], fmin=0, fmax=60, NFFT=64, plot=True)
    #psd = psd.squeeze()
    #print psd
    #print freqs
    # plt.plot(freqs, psd)
    mne.viz.plot_raw_psds(raw=raw, picks=[0], fmin=0, fmax=60, n_fft=128) # uses welch
    plt.show()

    events = mne.make_fixed_length_events(raw, 1, start=10.0, duration=1.0)
    print events
    epochs = mne.Epochs(raw, events, 1, tmin=-0.5, tmax=0.5, baseline=None)
    print epochs

    # mne.viz.plot_image_epochs(epochs, [0], sigma=0.5, colorbar=True, show=True)
    psds, freqs = mne.time_frequency.compute_epochs_psd(epochs, fmin=2, fmax=200, n_fft=256)

    average_psds = psds.mean(0)
    average_psds = 10 * np.log10(average_psds)  # transform into dB
    some_psds = 10 * np.log10(psds[12])
    
    fig, (ax1, ax2) = plt.subplots(1, 2, sharex=True, sharey=True, figsize=(10, 5))

    fig.suptitle('Single trial power', fontsize=12)

    freq_mask = freqs < 60
    freqs = freqs[freq_mask]

    ax1.set_title('single trial', fontsize=10)
    ax1.imshow(some_psds[:, freq_mask].T, aspect='auto', origin='lower')
    ax1.set_yticks(np.arange(0, len(freqs), 10))
    ax1.set_yticklabels(freqs[::10].round(1))
    ax1.set_ylabel('Frequency (Hz)')

    ax2.set_title('averaged over trials', fontsize=10)
    ax2.imshow(average_psds[:, freq_mask].T, aspect='auto', origin='lower')

    mne.viz.tight_layout()
    plt.show()
# def run_ica(raw):
#     ica = mne.preprocessing.ICA(n_components=.9)
#     ica.decompose_raw(raw, picks=[0], start=5.0, decim=3)
#     ica.detect_artifacts(raw)
#     print ica

#     ica.plot_sources_raw(raw, range(30), start=10., stop=15.)
    
                      
        
# def psd(raw):
#     fmin = 7
#     fmax = 20
#     tmin = 0.
#     tmax = 0.2
#     mt_bandwidth = 20

#     data_csd_mt = compute_csd(epochs, mode='multitaper', fmin=fmin, fmax=fmax,
#                               tmin=tmin, tmax=tmax, mt_bandwidth=mt_bandwidth)
#     data_csd_fourier = compute_csd(epochs, mode='fourier', fmin=fmin, fmax=fmax,
#                                    tmin=tmin, tmax=tmax)



#mne.filter.low_pass_filter()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Analyze and plot EEG data.')
    parser.add_argument('outDir',type=str, help='location of output files')
    parser.add_argument('filenames',type=str, nargs='+', help='the input EEG files in EDF formats')
    args = parser.parse_args()

    for filename in args.filenames:
        try:
            raw = read_raw_edf(filename, n_eeg=1, preload=True)
        except Exception, e:
            print "Error loading '%s'; ignoring" % filename

        print
        #ar_whiten(raw)
        plot_psd(raw)

        
        
    

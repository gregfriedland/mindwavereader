import os, itertools
import numpy
import pandas

import matplotlib
matplotlib.use("Agg")
import pylab
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.gridspec as gridspec
from pyPdf import PdfFileWriter, PdfFileReader

import pandas.rpy.common as com
from rpy2.robjects.packages import importr
import rpy2.robjects.lib.ggplot2 as ggplot2
from rpy2.robjects import r
ggsave = r["ggsave"]
from rpy2.robjects import Formula



COLORS = ["red", "darkorange", "gold", "lightgreen", "green", "blue", "purple", "lightgray", "darkgray", "black"]


def plotToFig(title, labels, xs, ys, xlim=None, ylim=None, xlabel="", ylabel="", colors=COLORS, linewidth=1, legend=True):
    assert(len(xs) == len(ys) == len(labels))
    
    fig = pylab.figure(figsize=(16,8), dpi=200)
    gs = gridspec.GridSpec(1, 1)
    axs = [fig.add_subplot(ss) for ss in gs]

    for label, x, y, color in zip(labels, xs, ys, itertools.cycle(colors)):
        axs[0].plot(x, y, color=color, label=label, linewidth=linewidth)

    if xlim:
        axs[0].set_xlim(xlim)
        
    if ylim == "fit":
        ymin = numpy.nanmin([numpy.nanmin(y) for y in ys])
        if not numpy.isfinite(ymin): ymin = 0
        ymax = numpy.nanmax([numpy.nanmax(y) for y in ys])                        
        if not numpy.isfinite(ymax): ymin = 0
        axs[0].set_ylim([ymin,ymax])
        #print [ymin, ymax]
    elif ylim:
        axs[0].set_ylim(ylim)

    axs[0].set_xlabel(xlabel)
    axs[0].set_ylabel(ylabel)

    if legend:
        handles, labels = axs[0].get_legend_handles_labels()
        axs[0].legend(handles, labels)

    fig.suptitle(title+"\n\n")
    gs.tight_layout(fig, rect=[0, 0.03, 1, 0.95])
    return fig



def plotBands(outDir, eegDatas, bandFreqs,
              begin, N, Fs, methods, movAvgLens):
    if not os.path.exists(outDir): os.makedirs(outDir)

    bands = bandFreqs.keys()
    marks = sorted(eegDatas.keys())

    for method in methods:
        for movAvgLen in movAvgLens:
            outFn = 'time_energies_by_band-method-%s_avg-%d.pdf'%(method, movAvgLen)
            with PdfPages(os.path.join(outDir, outFn)) as pdf:
                for band in bands:
                    energies = [eegDatas[mark].getAllMeanEnergies(bandFreqs[band], method, Fs, N, begin, movAvgLen) for mark in marks]
                    ts = [numpy.arange(float(begin), float(begin)+len(e)*N, N) / Fs for e in energies]

                    fig = plotToFig("%s: %s spectral density estimate" % (band, method),
                                                marks, ts, energies, xlabel="Seconds", ylabel=band, linewidth=3, ylim="fit")
                    pdf.savefig(fig)
                    pylab.close(fig)


                    
def plotPSDs(outDir, eegDatas, begin, N, Fs, methods):
    if not os.path.exists(outDir): os.makedirs(outDir)
                
    marks = sorted(eegDatas.keys())

    for mark in marks:
        outFn = os.path.join(outDir, "freq_energies_by_method-mark-%s.pdf" % (mark.replace(" ","_")))
        with PdfPages(outFn) as pdf:
            for method in methods:
                ts, freqs, psds = eegDatas[mark].getAllPSDs(method, Fs, N, begin)

                title = "Mark='%s'; Method=%s" % (mark, method)
                fig = plotToFig(title, ts, freqs, psds,ylim="fit", legend=False) # xlim=[0,MAX_FREQ],
                pdf.savefig(fig)
                pylab.close(fig)

                
def plotDistributions(outDir, eegDatas, bandFreqs, begin, N, Fs, methods, movAvgLens):
    if not os.path.exists(outDir): os.makedirs(outDir)

    bands = bandFreqs.keys()
    marks = sorted(eegDatas.keys())

    print "  Precalculating PSDs for all frames"
    df = pandas.DataFrame(columns=("Band", "Method", "MovAvgLen", "Mark", "Energy"), dtype=float)
    for band in bands:
        for method in methods:
            for movAvgLen in movAvgLens:
                for mark in marks:
                    energies = eegDatas[mark].getAllMeanEnergies(bandFreqs[band], method, Fs, N, begin, movAvgLen)
                    df = pandas.concat([df, pandas.DataFrame({"Band":band, "Method":method, "MovAvgLen":movAvgLen, "Mark":mark,
                                                              "Energy":energies})])

    print "  Calling ggplot2"
    outFn = os.path.join(outDir, 'distributions.pdf')

    rdf = com.convert_to_r_dataframe(df.reset_index())
    gp = ggplot2.ggplot(rdf)
    pp = gp + \
        ggplot2.aes_string(x='factor(Band)', y='Energy') + \
        ggplot2.scale_colour_brewer(palette = "Set1") + \
        ggplot2.scale_fill_brewer(palette = "Set1") + \
        ggplot2.geom_boxplot(ggplot2.aes_string(fill='factor(Mark)', color='factor(Mark)'), outlier_size = 1) + \
        ggplot2.facet_grid(Formula("Method ~ MovAvgLen"), scales="free_y") + \
        ggplot2.theme(**{'axis.text': ggplot2.element_text(size=30),
                         'axis.title': ggplot2.element_text(size=30,face="bold"),
                         'axis.text.x': ggplot2.element_text(angle=90, hjust=1, vjust=1, size=30),
                         'legend.text': ggplot2.element_text(size=30),
                         'legend.title': ggplot2.element_text(size=30),
                         'strip.text': ggplot2.element_text(size=30)})
    ggsave(outFn, pp, dpi=300, width=24, height=24)




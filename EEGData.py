import pandas, numpy, matplotlib
matplotlib.use("Agg")
import pylab, numpy
import sys, os
import collections
import itertools

import scipy.signal as sig
import scipy.stats.distributions as dist
import nitime.algorithms as tsa


def loadEEGDataFromCSV(fn, minSize, markNameMap={}):
    df = pandas.read_csv(fn,  dtype={'mark':str})

    marks = []
    mi = 0
    lastMark = ""
    for ind, row in df.iterrows():
        if row['mark'] != lastMark and isAscii(row['mark']):
            mi += 1
            lastMark = row['mark']

        newMark = "%d-%s" % (mi, markNameMap.get(lastMark, lastMark))
        marks.append(newMark)
    df['mark'] = marks

    eegDatas = {}
    marks = sorted(df.mark.unique())
    for mark in marks:
        rawEegs = df[df.mark == mark].rawEeg
        rawEegs = rawEegs[numpy.isfinite(rawEegs)]
        if len(rawEegs) >= minSize:
            eegDatas[mark] = EEGData(vals=rawEegs.astype(numpy.int32))
    
    return eegDatas



def isAscii(s):
    if isinstance(s, basestring):
        return s == "" or all(ord(c) < 128 for c in s)
    else:
        return False

            
def dB(x, out=None):
    if out is None:
        return 10 * numpy.log10(x)
    else:
        numpy.log10(x, out)
        numpy.multiply(out, 10, out)


# stores one block of data that canc calculate its PSD
class EEGDataBlock(object):
    def __init__(self, pos, vals):
        self._pos = pos
        self._vals = vals
        self._psd = {}


    def getPSD(self, method, Fs):
        if (method,Fs) not in self._psd:
            if method.startswith("lpasskaiser_"):
                method = method.replace("lpasskaiser_","")
            
                cutoff_hz = Fs / 4.0

                # create the low pass FIR filter
                nyq_rate = Fs / 2.0    # Nyquist rate
                width = 5.0 / nyq_rate # width of the transition in Hz
                ripple_db = 30.0       # attenuation of the stop band, in dB
                N, beta = sig.kaiserord(ripple_db, width) # order and Kaiser parameter
                taps = sig.firwin(N, cutoff_hz / nyq_rate, window=('kaiser', beta))
            
                # apply the filter and remove the 'corrupted' parts at the beginning
                vals = sig.lfilter(taps, 1.0, self._vals)
                vals = vals[N-1:]
                print "kaiser", len(self._vals), len(vals)
            elif method.startswith("lpasshamming_"):
                method = method.replace("lpasshamming_","")

                cutoff_hz = Fs / 4.0

                N = 79
                FC = cutoff_hz / (0.5 * Fs)
                fir = sig.firwin(N, cutoff=FC, window='hamming')
                vals = sig.lfilter(fir, 1, self._vals)
                vals = vals[N-1:]
                print "hamming", len(self._vals), len(vals)

            elif method.startswith("bpass_"):
                method = method.remove("bpass_")

            else:
                vals = self._vals

            if method == "ar":
                ak, sigma_v = tsa.AR_est_YW(vals, 8)
                freqs, psd = tsa.AR_psd(ak, sigma_v)
            elif method == "multitaper":
                freqs, psd = tsa.get_spectra(vals, method={'this_method': 'multi_taper_csd', 'Fs':Fs, 'adaptive':False, 'low_bias':False})
            elif method == "periodogram":
                freqs, psd = tsa.get_spectra(vals, method=dict(this_method='periodogram_csd', Fs=Fs, normalize=False))
            elif method == "fft":
                psd = numpy.abs(numpy.fft.rfft(vals))
                freqs = numpy.fft.rfftfreq(vals.size, 1.0/Fs)
            else:
                raise Exception("Unknown method: " + method)

            freqs = freqs * Fs/2 / freqs.max()
            psd = psd.squeeze()
            dB(psd, psd)
            psd = psd.astype(float)

            self._psd[(method,Fs)] = freqs, psd
            
        return self._psd[(method,Fs)]


    def getEnergy(self, freq, method, Fs):
        freqs, psd = self.getPSD(method, Fs)
        #print freqs
        #print psd
        #print
        return psd[freqs <= freq][-1]


    def getBandEnergies(self, freqBands, method, Fs):
        energies = []
        for band, freq in freqBands.iteritems():
            energies.append(self.getEnergy(freq, method, Fs))
        return numpy.array(energies)


    def __eq__(self, other):
        """ Dont' compare all the values in the interests of speed """
        return other is not None and self._pos == other._pos


    def __ne__(self, other):
        return not self == other

    
    def getVals(self):
        return self._vals



class EEGData(object):
    """ Stores one continuous stream of EEG data """
    def __init__(self, vals=[]):
        self._vals = numpy.asarray(vals)


    def append(self, vals):
        self._vals = numpy.append(self._vals, vals)


    def splitIntoBlocks(self, begin, N):
        for start in range(begin, len(self._vals)/N*N, N):
            yield EEGDataBlock(start, self._vals[start:start+N])


    def getLastEEGBlock(self, begin, N):
        if len(self._vals) < begin+N:
            return None
        
        for start in range(begin, len(self._vals)/N*N, N):
            pass
        return EEGDataBlock(start, self._vals[start:start+N])


    def getAllEnergies(self, freq, method, Fs, N, begin):
        es = []
        for block in self.splitIntoBlocks(begin, N):
            es.append(block.getEnergy(freq, method, Fs))
        return numpy.array(es)
        
        
    def getAllMeanEnergies(self, freq, method, Fs, N, begin, movAvgLen):
        es = self.getAllEnergies(freq, method, Fs, N, begin)
        if movAvgLen == 0:
            return es
        else:
            return pandas.rolling_mean(es, movAvgLen)
    
    
    def getAllPSDs(self, method, Fs, N, begin):
        freqs, psds, ts = [], [], []
        for bi, block in enumerate(self.splitIntoBlocks(begin, N)):
            freq, psd = block.getPSD(method, Fs)
            freqs.append(freq)
            psds.append(psd)
            ts.append(begin + bi*N)

        ts = numpy.array(ts)/Fs

        return ts, freqs, psds

    
    def getVals(self):
        return self._vals

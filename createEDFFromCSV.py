from EdfWriter import EdfWriter
from datetime import datetime
import numpy, pandas
import os, sys
import edflib._edflib as edf

def isAscii(s):
    if isinstance(s, basestring):
        return s == "" or all(ord(c) < 128 for c in s)
    else:
        return False

def parseCSV(fn):
    df = pandas.read_csv(fn)

    marks = []
    mi = 0
    lastMark = ""
    for ind, row in df.iterrows():
        if row['mark'] != lastMark and isAscii(row['mark']):
            mi += 1
            lastMark = row['mark']

        newMark = "%d-%s" % (mi,lastMark)
        marks.append(newMark)
    df['mark'] = marks

    vals = {}
    marks = sorted(df.mark.unique())
    for mark in marks:
        rawEegs = df[df.mark == mark].rawEeg
        rawEegs = rawEegs[numpy.isfinite(rawEegs)]
        vals[mark] = rawEegs.astype(numpy.int32)

    return vals

    
if __name__ == "__main__":
    infn = sys.argv[1]
    outDir = os.path.basename(infn).split(".")[0]
    if not os.path.exists(outDir):
        os.makedirs(outDir)
    
    vals = parseCSV(infn)

    channel_info = {"label": "MindWave", "dimension":"AU", "sample_rate":512,
                    "physical_max": 500, "physical_min": -500,                # these are arbitrary
                    "digital_max": 2**15-1, "digital_min": -2**15+1}

    for mark in vals.keys():
        outfn = os.path.join(outDir, mark + ".edf")
        print "Writing %d samples to %s" % (len(vals[mark]), outfn)
        
        writer = EdfWriter(outfn, [channel_info.copy()], file_type=edf.FILETYPE_BDFPLUS)

        # a bug in EdfWriter.__init__ clears the channel_info dict even though it is needed later, so restore it
        #writer.channels['MindWave'] = channel_info
        
        for val in vals[mark]:
            writer.write_sample("MindWave", val)

        writer.close()
        

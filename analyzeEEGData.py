from config import SAMPLE_RATE, MARK_NAME_MAP, BAND_FREQS, METHODS, NUM_STRIP_FRAMES, BLOCK_SIZE, MOV_AVG_LENS, MIN_NUM_FRAMES

import pandas, numpy, matplotlib
import sys, os
import itertools
import argparse

import scipy.signal as sig
import scipy.stats.distributions as dist
import nitime.algorithms as tsa

from EEGData import EEGData, loadEEGDataFromCSV
from PlotEEG import plotBands, plotPSDs, plotDistributions
        

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Analyze and plot EEG data.')
    parser.add_argument('filename',type=str, help='the input EEG data file')
    parser.add_argument('--plotPSDs', action='store_true', help='whether to plot the spectra densities')
    parser.add_argument('--plotDistributions', action='store_true', help='whether to plot the distributions of freqency band energies')
    parser.add_argument('--plotBands', action='store_true', help='whether to plot energies in the frequency bands over time')
    args = parser.parse_args()

    infn = sys.argv[1]
    outDir = os.path.basename(infn).split(".")[0]
    if not os.path.exists(outDir):
        os.makedirs(outDir)

    print "Loading EEG data"
    eegDatas = loadEEGDataFromCSV(infn, minSize=MIN_NUM_FRAMES, markNameMap=MARK_NAME_MAP)
    for mark, eegData in eegDatas.iteritems():
        fn = os.path.join(outDir, "eeg-%s.csv" % mark)
        pandas.Series(eegData.getVals(), name="rawEEG").to_csv(fn, header=True, index=False)


    if args.plotBands:
        print "Plotting Bands"
        plotBands(outDir, eegDatas, BAND_FREQS, begin=NUM_STRIP_FRAMES, N=BLOCK_SIZE, Fs=SAMPLE_RATE, methods=METHODS, movAvgLens=MOV_AVG_LENS)

    if args.plotPSDs:
        print "Plotting PSDs"
        plotPSDs(outDir, eegDatas, begin=NUM_STRIP_FRAMES, N=BLOCK_SIZE, Fs=SAMPLE_RATE, methods=METHODS)

    if args.plotDistributions:
        print "Plotting distributions"
        plotDistributions(outDir, eegDatas, BAND_FREQS, begin=NUM_STRIP_FRAMES, N=BLOCK_SIZE, Fs=SAMPLE_RATE, movAvgLens=MOV_AVG_LENS, methods=METHODS)

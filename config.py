import collections

SAMPLE_RATE = 512
BLOCK_SIZE = SAMPLE_RATE
NUM_STRIP_FRAMES = int(5.0 * SAMPLE_RATE) # how many frames to strip from the front and the end of the readings
MIN_NUM_FRAMES = NUM_STRIP_FRAMES*3

METHODS = (#"fft", "lpasskaiser_fft", "lpasshamming_fft",
           "lpasskaiser_ar",
           "lpasskaiser_multitaper") #, "periodogram", "fft")
VIZ_METHOD = "ar"

MOV_AVG_LENS = [0,10,40]

OUTPUT_DATA_FILE = "mindwave.out"

MARK_NAME_MAP = {"m":"breathing meditation",
               "c":"counting",
               "r":"reading internet news",
               "z":"reading zite",
               "g":"playing a game",
               "p":"programming",
               "l":"blinking",
               "n":"noting meditation",
               "e":"eyes open baseline",
               "b":"eyes closed baseline"}

BAND_FREQS = collections.OrderedDict([("02Hz delta",2), ("05Hz theta",5), ("08Hz lowAlpha",8),
                                      ("11Hz highAlpha",11), ("15Hz lowBeta",15), ("24Hz highBeta", 24),
                                      ("35Hz lowGamma", 35), ("45Hz highGamma", 45)])

BAND_PLOT_RANGES = collections.OrderedDict([("02Hz delta",(50,70)), ("05Hz theta",(45,65)),
                                            ("08Hz lowAlpha",(40,60)), ("11Hz highAlpha",(35,60)),
                                            ("15Hz lowBeta",(35,55)), ("24Hz highBeta", (30,50)),
                                            ("35Hz lowGamma", (30,50)), ("45Hz highGamma", (25,45))])

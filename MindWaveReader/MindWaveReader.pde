import org.json.JSONObject;
import org.json.JSONException;
import processing.net.*;

import ddf.minim.*;
import ddf.minim.ugens.*;

Client myClient; 

Minim       minim;
AudioOutput out;
Oscil       wave;

PrintWriter output;
String mark = "";

ArrayList<String> fields;
ArrayList<String> eegFields;


void setup() {
  size(200, 200);
  smooth();
  
  // Connect to ThinkGear socket (default = 127.0.0.1:13854)
  // By default, Thinkgear only binds to localhost:
  // To allow other hosts to connect and run Processing from another machine, run ReplayTCP (http://www.dlcsistemas.com/html/relay_tcp.html)
  // OR, use netcat (windows or mac) to port forard (clients can now connect to port 13855).  Ex:  nc -l -p 13855 -c ' nc localhost 13854'
  
  String thinkgearHost = "127.0.0.1";
  int thinkgearPort = 13854;
  
  String envHost = System.getenv("THINKGEAR_HOST");
  if (envHost != null) {
    thinkgearHost = envHost;
  }
  String envPort = System.getenv("THINKGEAR_PORT");
  if (envPort != null) {
     thinkgearPort = Integer.parseInt(envPort);
  }
 
  println("Connecting to host = " + thinkgearHost + ", port = " + thinkgearPort);
  myClient = new Client(this, thinkgearHost, thinkgearPort);
  String command = "{\"enableRawOutput\": true, \"format\": \"Json\"}\n";
  print("Sending command");
  println (command);
  myClient.write(command);
 
 background(0);
 
//  minim = new Minim(this);
//  out = minim.getLineOut();
//  wave = new Oscil( 440, 0.3f, Waves.SINE );
//  wave.patch( out );

  output = createWriter("mindwave.out"); 
  output.println("rawEeg,attention,meditation,delta,theta,lowAlpha,highAlpha,lowBeta,highBeta,lowGamma,highGamma,mark");
  output.flush();  
}

void draw() {
 if (myClient.available() > 0) {
    String data = myClient.readString();
    try {
      print("JSON:" + data + "\n\n");
      JSONObject json = new JSONObject(data);
      
      // display signal level
      int signal = json.getInt("poorSignalLevel");
      color signalColor = 0;
      if (signal == 200) signalColor = color(255,255,255);
      else if (signal == 0) signalColor = color(0,255,0);
      else if (signal < 200) signalColor = color(255,0,0);
      fill(signalColor);
      ellipse(100,100,50,50);

      HashMap<String,Integer> map = new HashMap<String,Integer>();
      if (json.has("eSense")) {
        JSONObject esense = json.getJSONObject("eSense"); // "attention", "meditation" 
        
        map.put("attention", esense.getInt("attention"));
        map.put("meditation", esense.getInt("attention"));
      }
      
      if (json.has("eegPower")) {
        JSONObject eeg = json.getJSONObject("eegPower");// "delta", "theta", "lowAlpha", "highAlpha", "lowBeta", "highBeta", "lowGamma", "highGamma"
        map.put("delta", eeg.getInt("delta"));
        map.put("theta", eeg.getInt("theta"));
        map.put("lowAlpha", eeg.getInt("lowAlpha"));
        map.put("highAlpha", eeg.getInt("highAlpha"));
        map.put("lowBeta", eeg.getInt("lowBeta"));
        map.put("highBeta", eeg.getInt("highBeta"));
        map.put("lowGamma", eeg.getInt("lowGamma"));
        map.put("highGamma", eeg.getInt("highGamma"));
      }
      
      if (json.has("rawEeg")) {
        map.put("rawEeg", json.getInt("rawEeg"));
      }
      
      //wave.setFrequency( 220 + 3*esense.getInt("meditation") );
        
      output.println(map.get("rawEeg") + "," +
                     map.get("meditation") + "," +
                     map.get("attention") + "," + 
                     map.get("delta") + "," + 
                     map.get("theta") + "," + 
                     map.get("lowAlpha") + "," + 
                     map.get("highAlpha") + "," + 
                     map.get("lowBeata") + "," + 
                     map.get("highBeta") + "," + 
                     map.get("lowGamma") + "," + 
                     map.get("highGamma") + "," +
                     mark);
     output.flush();

      
    } catch (JSONException e) {
      println ("There was an error parsing the JSONObject." + e);
    };
 }

}


void keyReleased() {
  mark = ""+key;
}

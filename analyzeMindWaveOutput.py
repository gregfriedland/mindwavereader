# delta: adult slow-wave sleep, in babies, has been found during some continuous-attention tasks.

# theta: young children, drowsiness or arousal in older children and adults, idling, associated with inhibition of elicited responses (has been found to spike in situations where a person is actively trying to repress a response or action).

# alpha: relaxed/reflecting, closing the eyes, also associated with inhibition control, seemingly with the purpose of timing inhibitory activity in different locations across the brain.

# beta: alert, active, busy, or anxious thinking, active concentration.

# gamma: displays during cross-modal sensory processing (perception that combines two different senses, such as sound and sight), also is shown during short-term memory matching of recognized objects, sounds, or tactile sensations.


import pandas, numpy, matplotlib
matplotlib.use("Agg")
import pylab, numpy
import sys, os
import collections
import itertools, operator

from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.gridspec as gridspec

import scipy.signal as sig
import scipy.stats.distributions as dist

import nitime.algorithms as tsa
#import nitime.utils as utils
#from nitime.viz import winspect
#from nitime.viz import plot_spectral_estimate

import argparse

def chunks(l, n):
    """ Yield successive n-sized chunks from l.
    """
    for i in xrange(0, len(l), n):
        yield l[i:i+n]
        

MIN_RAW_EEGS = 500
MAX_FREQ = 60
COLORS = ["red", "darkorange", "gold", "lightgreen", "green", (0,0,1), "purple", "lightgray", "darkgray", "black"]
COLORS = COLORS*10

def plotToFig(title, labels, xs, ys, xlim=None, ylim=None, xlabel="", ylabel="", colors=COLORS, linewidth=1, legend=True):
    assert(len(colors) >= len(xs))
    assert(len(xs) == len(ys) == len(labels))

    fig = pylab.figure(figsize=(16,8), dpi=200)
    gs = gridspec.GridSpec(1, 1)
    axs = [fig.add_subplot(ss) for ss in gs]

    #labels, xs, ys = zip(*sorted(zip(labels, xs, ys))) # sort by label name
    for label, x, y, color in zip(labels, xs, ys, colors):
        axs[0].plot(x, y, color=color, label=label, linewidth=linewidth)

    if xlim:
        axs[0].set_xlim(xlim)
    if ylim == "fit":
        #print labels
        #print ys
        #print
        
        ymin = numpy.nanmin([numpy.nanmin(y) for y in ys])                        
        ymax = numpy.nanmax([numpy.nanmax(y) for y in ys])                        
        axs[0].set_ylim([ymin,ymax])
    elif ylim:
        axs[0].set_ylim(ylim)

    axs[0].set_xlabel(xlabel)
    axs[0].set_ylabel(ylabel)

    if legend:
        handles, labels = axs[0].get_legend_handles_labels()
        #labels, handles = zip(*sorted(zip(labels, handles)))
        axs[0].legend(handles, labels)

    fig.suptitle(title+"\n\n")
    gs.tight_layout(fig, rect=[0, 0.03, 1, 0.95])
    return fig

            
def dB(x, out=None):
    if out is None:
        return 10 * numpy.log10(x)
    else:
        numpy.log10(x, out)
        numpy.multiply(out, 10, out)

        
class EEGData(object):
    def __init__(self, filename, modeNameMap, bandFreqs, minModeSize=60):
        self._bandFreqs = bandFreqs
        self._df = pandas.read_csv(filename, dtype={'mark':str})
        self._data = self._parse(self._df, modeNameMap, minModeSize)
        self._calcSpectralDensities()

        self._psds, self._meanPsds = {}, {}

        
    def _parse(self, df, modeNameMap, minModeSize=60):
        """
        Return data is in the format: dict(mode->dict("time"->float, "mark"->string, "rawEegs"->ndarray, "delta"->int, ...))
        """
        print "Columns found: %s" % (" ".join(df.columns))
        df = df.fillna("")

        if 'time' not in df.columns:
            df['time'] = numpy.arange(df.shape[0])
            
        # update the mode names to be pretty and numbered
        #isNewMark = [True] + ((df.mark[1:] != df.mark[:-1]) & (df.mark[1:] != ""))
        
        modes = []
        mi = 0
        lastMode = ""
        for ind, row in df.iterrows():
            if row['mark'] != lastMode and isValidMode(row['mark']):
                mi += 1
                lastMode = row['mark']
                
            newMark = "%d-%s" % (mi,modeNameMap.get(lastMode, lastMode))
            modes.append(newMark)
        df['mark'] = modes
        
        modesOrder = df.mark.drop_duplicates()
        print "Modes found:", list(modesOrder)
        
        allData = {}
        for mi, mode in enumerate(modesOrder):
            subdf = df[df.mark==mode][:]
            subdf['time'] = subdf['time'].values - list(subdf['time'])[0]

            # append all the rawEeg rows between the rows with eegPower (delta, theta, etc) (should be 512)            
            allData[mode] = []
            rawEegs = []

            for ind, row in subdf.iterrows():
                row = dict(row.iteritems())
                if "rawEeg" in row and row["rawEeg"] != "":
                    rawEegs.append(int(row["rawEeg"]))
                elif row["theta"] != "":
                    n = len(rawEegs)
                    if n < MIN_RAW_EEGS:
                        print "\tFound %d rawEegs; skipping" % (n)
                        row["rawEegs"] = numpy.array([])
                    else:
                        row["rawEegs"] = numpy.array(rawEegs)
                    allData[mode].append(row)
                    rawEegs = []
                else:
                    raise Exception("Unexpected row: %s" % row)

            # don't include modes without much data
            if len(allData[mode]) < minModeSize:
                del allData[mode]
            else:
                print "\nMode='%s' (%d)" % (mode, len(allData[mode]))


        return allData

    
    def _calcSpectralDensities(self):
        for mode, rows in self._data.items():
            for row in rows:
                rawEegs = row["rawEegs"]
                N = len(rawEegs)
                if N == 0: continue

                # Welch method: average over sliding window
                welch_n = N/2
                welch_freqs, welch_psd = tsa.get_spectra(rawEegs, method=dict(this_method='welch', NFFT=welch_n, Fs=N))
                welch_psd = welch_psd.squeeze()
                dB(welch_psd, welch_psd)
                row["psd_welch"] = (welch_freqs, welch_psd)

                # Periodogram
                period_freqs, period_psd = tsa.get_spectra(rawEegs, method=dict(this_method='periodogram_csd', Fs=N, normalize=False))
                period_freqs = period_freqs * N/2 / period_freqs.max()
                period_psd = period_psd.squeeze()
                dB(period_psd, period_psd)
                row["psd_periodogram"] = (period_freqs, period_psd)

                # AR method
                ak, sigma_v = tsa.AR_est_YW(rawEegs, 8)
                ar_freqs, ar_psd = tsa.AR_psd(ak, sigma_v)
                ar_freqs = ar_freqs * N/2 / ar_freqs.max()
                ar_psd = ar_psd.squeeze()
                dB(ar_psd, ar_psd)
                row["psd_ar"] = (ar_freqs, ar_psd)
                # print ar_freqs
                # print ar_psd
                # print
                
                # Multitaper method
                taper_freqs, taper_psd = tsa.get_spectra(rawEegs, method={'this_method': 'multi_taper_csd', 'Fs':N, 'adaptive':True, 'low_bias':False})
                taper_psd = taper_psd.squeeze()
                taper_freqs = taper_freqs * N/2 / taper_freqs.max()
                dB(taper_psd, taper_psd)
                row["psd_multitaper"] = (taper_freqs, taper_psd)

                # Multitaper method split into 3 and averaged
                for chunk_size in (4,):
                    taper_psds = []
                    for vals in chunks(rawEegs[:N/chunk_size*chunk_size], N/chunk_size):
                        taper_freqs, taper_psd = tsa.get_spectra(vals, method={'this_method': 'multi_taper_csd', 'Fs':N, 'adaptive':True, 'low_bias':False})
                        taper_psd = taper_psd.squeeze()
                        taper_freqs = taper_freqs * N/2/chunk_size / taper_freqs.max()
                        dB(taper_psd, taper_psd)
                        taper_psds.append(taper_psd)
                    taper_psd = reduce(operator.add, taper_psds) / len(taper_psds)
                    row["psd_multitaper_averaged%d"%chunk_size] = (taper_freqs, taper_psd)

                # taper_freqs, taper_psd = tsa.get_spectra(rawEegs, method={'this_method': 'multi_taper_csd', 'Fs':N, 'adaptive':False, 'low_bias':False})
                # taper_psd = taper_psd.squeeze()
                # taper_freqs = taper_freqs * N/2 / taper_freqs.max()
                # dB(taper_psd, taper_psd)
                # row["psd_multitaper_nonadaptive"] = (taper_freqs, taper_psd)

                # taper_freqs, taper_psd = tsa.get_spectra(rawEegs, method={'this_method': 'multi_taper_csd', 'Fs':N, 'adaptive':False, 'low_bias':True})
                # taper_psd = taper_psd.squeeze()
                # taper_freqs = taper_freqs * N/2 / taper_freqs.max()
                # dB(taper_psd, taper_psd)
                # row["psd_multitaper_nonadaptive_lowbias"] = (taper_freqs, taper_psd)

                # FFT
                fft_psd = numpy.abs(numpy.fft.rfft(rawEegs))
                dB(fft_psd, fft_psd)
                fft_freqs = numpy.fft.rfftfreq(rawEegs.size, 1.0/N)
                row["psd_fft"] = (fft_freqs, fft_psd)

                # FFT with Hann window
                fft_hann_psd = numpy.abs(numpy.fft.rfft(rawEegs * numpy.hanning(N)))
                dB(fft_hann_psd, fft_hann_psd)
                row["psd_hann_fft"] = (fft_freqs, fft_hann_psd)

        #         print row.keys()
        # keys = set()
        # for mode, rows in self._data.items():
        #     for row in rows:
        #         keys = keys.union(row.keys())
        # print keys
        # print


    def plotSpectralDensities(self, outDir):
        if not os.path.exists(outDir): os.makedirs(outDir)
                    
        modes = self._data.keys()
        psd_methods = set([k for mode in modes for row in self._data[mode] for k in row.keys() if k.startswith("psd_")])
        #ymax = numpy.array([row[psd_method][1].max() for psd_method in psd_methods for mode,rows in self._data.iteritems() for row in rows]).max()

        # create PDF with one image per page for each timepoint, comparing psd methods on the same image
        for mode, rows in self._data.iteritems():
            fn = os.path.join(outDir, "psd-bytime-%s.pdf" % (mode.replace(" ","_")))
            with PdfPages(fn) as pdf:
                for frame, row in enumerate(rows):
                    freqs = [self._getFullFreqsOverTime(mode, psd_method, frame) for psd_method in psd_methods]
                    psds = [self._getFullPSDsOverTime(mode, psd_method, frame) for psd_method in psd_methods]
                    freqs = [f for f in freqs if len(f) > 0]
                    psds = [p for p in psds if len(p) > 0]
                                            
                    # skip if no data exists for this params
                    if len(psds) == 0 or len(numpy.concatenate(psds)) == 0: continue

                    title = "Mode='%s'; Frame=%d; Time=%.1f" % (mode, frame, row['time'])
                    fig = plotToFigWithLegend(title, psd_methods, freqs, psds, xlim=[0,MAX_FREQ], ylim="fit")
                    pdf.savefig(fig)
                    pylab.close(fig)

        # create PDF with one image per page for each psd_method, comparing timepoints on the same image
        for mode, rows in self._data.iteritems():
            fn = os.path.join(outDir, "psd-bymethod-%s.pdf" % (mode.replace(" ","_")))
            with PdfPages(fn) as pdf:               
                for psd_method in psd_methods:
                    freqs = [self._getFullFreqsOverTime(mode, psd_method, frame) for frame in range(len(rows))]
                    psds = [self._getFullPSDsOverTime(mode, psd_method, frame) for frame in range(len(rows))]

                    freqs = [f for f in freqs if len(f) > 0]
                    psds = [p for p in psds if len(p) > 0]
                    #times = ["%.0f"%row["time"] for row in rows]
                    times = range(len(freqs))
                                            
                    # skip if no data exists for this params
                    if len(psds) == 0 or len(numpy.concatenate(psds)) == 0: continue
                    
                    # get the standard devs for each freq
                    n_freq = min([len(freq[freq<=MAX_FREQ]) for freq in freqs]) # find the number of freq entries to include under the MAX_FREQ
                    all_psds = [psd[:n_freq] for psd in psds]
                    
                    all_psds = numpy.vstack(all_psds)
                    stds_within_freq = all_psds.std(axis=0)
                    stds_within_freq = numpy.percentile(stds_within_freq, [10,25,50,75,90])
                    stds_within_freq = " ".join(["%.1f"%q for q in stds_within_freq])

                    title = "Mode='%s'; PSD method=%s\nWithin freq stds quantiles = %s" % (mode, psd_method, stds_within_freq)
                    fig = plotToFigWithLegend(title, times, freqs, psds, xlim=[0,75], ylim="fit")
                    pdf.savefig(fig)
                    pylab.close(fig)

                    
    def _getFullPSDsOverTime(self, mode, psd_method, frame):
        rows = self._data[mode]
        if psd_method not in rows[frame]:
            return numpy.array([])
        else:
            return rows[frame][psd_method][1]

    
    def _getFullFreqsOverTime(self, mode, psd_method, frame):
        rows = self._data[mode]
        if psd_method not in rows[frame]:
            return numpy.array([])
        else:
            return rows[frame][psd_method][0] 

    
    def _getPSDByBand(self, mode, band, psd_method):
        key = (mode, band, psd_method)
        if key not in self._psds:
            rows = self._data[mode]

            vals = []
            for row in rows:
                if psd_method.startswith("psd_") and psd_method in row:
                    freqs, psds = row[psd_method]
                    val = psds[freqs <= self._bandFreqs[band]][-1]
                    vals.append(val)
                elif psd_method == "mindwave":
                    vals.append(row[band])

            self._psds[key] = numpy.array(vals)
        return self._psds[key]

    
    def _getMeanPSDByBand(self, mode, band, psd_method, movAvgLength):
        key = (mode, band, psd_method, movAvgLength)
        if key not in self._meanPsds:
            psd = self._getPSDByBand(mode, band, psd_method)
            self._meanPsds[key] = pandas.rolling_mean(psd, movAvgLength)
        return self._meanPsds[key]


    def plotBands(self, outDir, movAvgLengths=[1,10,30,75], psd_methods=("psd_multitaper_averaged4","mindwave")):
        if not os.path.exists(outDir): os.makedirs(outDir)

        bands = self._bandFreqs.keys()
        modes = sorted(self._data.keys())

        for psd_method in psd_methods:
            for movAvgLength in movAvgLengths:
                with PdfPages(os.path.join(outDir, 'freq_byband_%s_avg%d.pdf'%(psd_method, movAvgLength))) as pdf:
                    for band in bands:
                        #xs = [[row['time'] for row in self._data[mode]] for mode in modes]
                        ys = [self._getMeanPSDByBand(mode, band, psd_method, movAvgLength) for mode in modes]
                        xs = [numpy.arange(len(y)) for y in ys]
                        
                        # skip if no data exists for this params
                        if len(numpy.concatenate(ys)) == 0: continue
                        
                        fig = plotToFigWithLegend("%s (%.0fHz): %s spectral density estimate" % (band, self._bandFreqs[band], psd_method),
                                                  modes, xs, ys, xlabel="Seconds", ylabel=band, linewidth=3, ylim="fit")
                        pdf.savefig(fig)
                        pylab.close(fig)

                with PdfPages(os.path.join(outDir, 'freq_bymode_%s_avg%d.pdf'%(psd_method, movAvgLength))) as pdf:
                    for mode in modes:
                        #xs = [[row['time'] for row in self._data[mode]] for band in bands]
                        ys = [self._getMeanPSDByBand(mode, band, psd_method, movAvgLength) for band in bands]
                        xs = [numpy.arange(len(y)) for y in ys]                        

                        # skip if no data exists for this params
                        if len(numpy.concatenate(ys)) == 0: continue
                        
                        fig = plotToFigWithLegend("%s: %s spectral density estimate" % (mode, psd_method),
                                                  bands, xs, ys, xlabel="Seconds", ylabel=mode, linewidth=3, ylim="fit")
                        pdf.savefig(fig)
                        pylab.close(fig)

        
def isValidMode(mode):
    if mode in (None, numpy.nan, ""):
        return False
    elif isinstance(mode, basestring):
        isascii = all(ord(c) < 128 for c in mode)
        return isascii
    else:
        raise ValueError("Unexpected mode '%s'" % mode)




# def makeFigure(datas, labels, xlabel, ylabel):
#     colors = ["red", "darkorange", "gold", "lightgreen", "green", (0,0,1), "purple", "lightgray", "darkgray", "black"]
#     assert(len(colors) >= len(datas))
#     assert(len(datas) == len(labels))

#     fig = pylab.figure(figsize=(16, 8), dpi=200)
#     ax = fig.add_subplot(111)
#     for data, label, color in zip(datas, labels, colors):
#         ax.plot(data, color=color, label=label, linewidth=3)
#     ax.set_xlabel(xlabel)
#     ax.set_ylabel(ylabel)

#     handles, labels = ax.get_legend_handles_labels()
#     labels, handles = zip(*sorted(zip(labels, handles)))
#     fig.legend(handles, labels)

#     fig.tight_layout()
#     return fig


# plot timeseries for each channel and a moving average
# # broken up by mode
# def plot(dfs, outDir, movingAvgWindow=20):    
#     channels = sorted(dfs.values()[0].columns)
#     modes = sorted(dfs.keys())

#     with PdfPages(os.path.join(outDir, 'bychannel.pdf')) as pdf:
#         for channel in channels:
#             avgDatas = [pandas.rolling_mean(dfs[mode][channel], movingAvgWindow) for mode in modes]
#             fig = makeFigure(avgDatas, modes, "Seconds", channel)
#             pdf.savefig(fig)

#     with PdfPages(os.path.join(outDir, 'bymode.pdf')) as pdf:
#         for mode in modes:
#             avgDatas = [pandas.rolling_mean(dfs[mode][channel], movingAvgWindow) for channel in channels]
#             fig = makeFigure(avgDatas, channels, "Seconds", mode)
#             pdf.savefig(fig)


# perform tests to compare the values of each channel between modes
# after trimming the beginning and end
# def compareModes(df, trimEntries=30):
#    pass


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Analyze and plot EEG data.')
    parser.add_argument('filename',type=str, help='the input EEG data file')
    parser.add_argument('--plotSpectralDensity', action='store_true', help='whether to plot the spectra densities')
    parser.add_argument('--plotBands', action='store_true', help='whether to plot frequency bands over time')
    args = parser.parse_args()

    name = os.path.basename(args.filename[:-4])

    modeNameMap = {"m":"breathing meditation",
                   "c":"counting",
                   "r":"reading internet news",
                   "z":"reading zite",
                   "g":"playing a game",
                   "p":"programming",
                   "l":"led glasses",
                   "n":"noting meditation",
                   "e":"eyes open baseline",
                   "b":"eyes closed baseline"}

    # channelNameMap = {"delta":"a-delta",
    #                   "theta":"b-theta",
    #                   "lowAlpha":"c-lowAlpha",
    #                   "highAlpha":"d-highAlpha",
    #                   "lowBeta":"e-lowBeta",
    #                   "highBeta":"f-highBeta",
    #                   "lowGamma":"g-lowGamma",
    #                   "highGamma":"h-highGamma",
    #                   "meditation":"i-meditation",
    #                   "attention":"j-attention"}

    bandFreqs = collections.OrderedDict([("delta",2), ("theta",5), ("lowAlpha",8), ("highAlpha",11), ("lowBeta",15),
                                         ("highBeta", 24), ("lowGamma", 35), ("highGamma", 45)])
    
    # clean up data and split it by mode
    eegData = EEGData(args.filename, modeNameMap, bandFreqs, minModeSize=40)

    if args.plotSpectralDensity:
        print "Plotting Spectral Densities"
        eegData.plotSpectralDensities(name)

    if args.plotBands:
        print "Plotting Bands"
        eegData.plotBands(name, psd_methods=("psd_fft", "psd_multitaper", "psd_ar", "psd_multitaper_averaged4","mindwave"))
        
    # # plot data
    # outDir = name
    # if not os.path.exists(outDir):
    #     os.makedirs(outDir)
    # plot(dfs, outDir, 75)

    # # analyze stats
    # compareModes(df)

import pandas, numpy, matplotlib
matplotlib.use("Agg")
import pylab, numpy
import sys, os
import collections
import itertools
import argparse

from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.gridspec as gridspec
from pyPdf import PdfFileWriter, PdfFileReader

import scipy.signal as sig
import scipy.stats.distributions as dist
import nitime.algorithms as tsa

import pandas.rpy.common as com
from rpy2.robjects.packages import importr
import rpy2.robjects.lib.ggplot2 as ggplot2
from rpy2.robjects import r
ggsave = r["ggsave"]
from rpy2.robjects import Formula
import rpy2.robjects as robj


def isAscii(s):
    if isinstance(s, basestring):
        return s == "" or all(ord(c) < 128 for c in s)
    else:
        return False

            
def dB(x, out=None):
    if out is None:
        return 10 * numpy.log10(x)
    else:
        numpy.log10(x, out)
        numpy.multiply(out, 10, out)

        
START_SECS = 5.0
SAMPLE_RATE = 512

COLORS = ["red", "darkorange", "gold", "lightgreen", "green", "blue", "purple", "lightgray", "darkgray", "black"]


def plotToFig(title, labels, xs, ys, xlim=None, ylim=None, xlabel="", ylabel="", colors=COLORS, linewidth=1, legend=True):
    #assert(len(colors) >= len(xs))
    assert(len(xs) == len(ys) == len(labels))
    
    fig = pylab.figure(figsize=(16,8), dpi=200)
    gs = gridspec.GridSpec(1, 1)
    axs = [fig.add_subplot(ss) for ss in gs]

    for label, x, y, color in zip(labels, xs, ys, itertools.cycle(colors)):
        axs[0].plot(x, y, color=color, label=label, linewidth=linewidth)

    if xlim:
        axs[0].set_xlim(xlim)
        
    if ylim == "fit":
        ymin = numpy.nanmin([numpy.nanmin(y) for y in ys])
        if not numpy.isfinite(ymin): ymin = 0
        ymax = numpy.nanmax([numpy.nanmax(y) for y in ys])                        
        if not numpy.isfinite(ymax): ymin = 0
        axs[0].set_ylim([ymin,ymax])
        #print [ymin, ymax]
    elif ylim:
        axs[0].set_ylim(ylim)

    axs[0].set_xlabel(xlabel)
    axs[0].set_ylabel(ylabel)

    if legend:
        handles, labels = axs[0].get_legend_handles_labels()
        axs[0].legend(handles, labels)

    fig.suptitle(title+"\n\n")
    gs.tight_layout(fig, rect=[0, 0.03, 1, 0.95])
    return fig


class EEGData(object):
    def __init__(self, filename, markNameMap, bandFreqs, minSize=1024):
        self._bandFreqs = bandFreqs
        self._vals = self._parseCSV(filename, markNameMap, minSize)
        self._psds = {}

                                    
    def _parseCSV(self, fn, markNameMap, minSize):
        df = pandas.read_csv(fn,  dtype={'mark':str})

        marks = []
        mi = 0
        lastMark = ""
        for ind, row in df.iterrows():
            if row['mark'] != lastMark and isAscii(row['mark']):
                mi += 1
                lastMark = row['mark']

            newMark = "%d-%s" % (mi,markNameMap.get(lastMark, lastMark))
            marks.append(newMark)
        df['mark'] = marks

        vals = {}
        marks = sorted(df.mark.unique())
        for mark in marks:
            rawEegs = df[df.mark == mark].rawEeg
            rawEegs = rawEegs[numpy.isfinite(rawEegs)]
            if len(rawEegs) >= minSize:
                vals[mark] = rawEegs.astype(numpy.int32)

        return vals


    def _getPSD(self, mark, method, start, N, Fs):
        key = (mark, method, start, N, Fs)
        if key not in self._psds:
            print "Calculating PSD for: %s" % (str(key))
            
            vals = self._vals[mark][start:start+N]
            vals = vals.values

            if method == "ar":
                ak, sigma_v = tsa.AR_est_YW(vals, 8)
                freqs, psd = tsa.AR_psd(ak, sigma_v)
            elif method == "multitaper":
                #freqs, psd = tsa.get_spectra(vals, method={'this_method': 'multi_taper_csd', 'Fs':Fs, 'adaptive':True, 'low_bias':False})
                freqs, psd = tsa.get_spectra(vals, method={'this_method': 'multi_taper_csd', 'Fs':Fs, 'adaptive':False, 'low_bias':False})
            elif method == "periodogram":
                freqs, psd = tsa.get_spectra(vals, method=dict(this_method='periodogram_csd', Fs=Fs, normalize=False))
            elif method == "fft":
                psd = numpy.abs(numpy.fft.rfft(vals))
                freqs = numpy.fft.rfftfreq(vals.size, 1.0/Fs)

            freqs = freqs * Fs/2 / freqs.max()
            psd = psd.squeeze()
            dB(psd, psd)
            psd = psd.astype(float)

            self._psds[key] = freqs, psd

        return self._psds[key]


    def _getAllEnergies(self, mark, method, begin, N, Fs):
        freqs, psds = [], []
        for start in range(begin, len(self._vals[mark])/N*N, N): # do even blocks of size N
            freq, psd = self._getPSD(mark, method, start, N, Fs)
            freqs.append(freq)
            psds.append(psd)

        ts = numpy.arange(float(begin), len(self._vals[mark]), N) / Fs

        return ts, freqs, psds
        
    
    def _getAllEnergiesForBand(self, mark, band, method, begin, N, Fs):
        vals = []
        for start in range(begin, len(self._vals[mark])/N*N, N):
            freqs, psd = self._getPSD(mark, method, start, N, Fs)

            val = psd[freqs <= self._bandFreqs[band]][-1]
            vals.append(val)

        return numpy.array(vals)


    def _getAllMeanEnergiesForBand(self, mark, band, method, begin, N, Fs, movAvgLen):
        vals = self._getAllEnergiesForBand(mark, band, method, begin, N, Fs)
        if movAvgLen == 0:
            return vals
        else:
            return pandas.rolling_mean(vals, movAvgLen)


    def plotBands(self, outDir, begin, N, Fs, movAvgLens=[0,10,30,75], methods=("ar", "multitaper", "periodogram", "fft")):
        if not os.path.exists(outDir): os.makedirs(outDir)

        bands = self._bandFreqs.keys()
        marks = sorted(self._vals.keys())

        for method in methods:
            for movAvgLen in movAvgLens:
                with PdfPages(os.path.join(outDir, 'time_energies_by_band-method-%s_avg-%d.pdf'%(method, movAvgLen))) as pdf:
                    for band in bands:
                        energies = [self._getAllMeanEnergiesForBand(mark, band, method, begin, N, Fs, movAvgLen) for mark in marks]
                        ts = [numpy.arange(float(begin), float(begin)+len(e)*N, N) / Fs for e in energies]

                        fig = plotToFig("%s: %s spectral density estimate" % (band, method),
                                                    marks, ts, energies, xlabel="Seconds", ylabel=band, linewidth=3, ylim="fit")
                        pdf.savefig(fig)
                        pylab.close(fig)

                        
    def plotPSDs(self, outDir, begin, N, Fs, methods=("ar", "multitaper", "periodogram", "fft")):
        if not os.path.exists(outDir): os.makedirs(outDir)
                    
        marks = sorted(self._vals.keys())

        for mark in marks:
            fn = os.path.join(outDir, "freq_energies_by_method-mark-%s.pdf" % (mark.replace(" ","_")))
            with PdfPages(fn) as pdf:
                for method in methods:
                    ts, freqs, psds = self._getAllEnergies(mark, method, begin, N, Fs)

                    title = "Mark='%s'; Method=%s" % (mark, method)
                    fig = plotToFig(title, ts, freqs, psds,ylim="fit", legend=False) # xlim=[0,MAX_FREQ],
                    print
                    pdf.savefig(fig)
                    pylab.close(fig)

                    
    def plotDistributions(self, outDir, begin, N, Fs, movAvgLens=[0,10,30,75], methods=("ar", "multitaper", "periodogram", "fft"), colors=COLORS):
        if not os.path.exists(outDir): os.makedirs(outDir)

        bands = self._bandFreqs.keys()
        marks = sorted(self._vals.keys())

        print "Calculating PSDs for all frames"
        df = pandas.DataFrame(columns=("Band", "Method", "MovAvgLen", "Mark", "Energy"), dtype=float)
        for band in bands:
            for method in methods:
                for movAvgLen in movAvgLens:
                    for mark in marks:
                        energies = self._getAllMeanEnergiesForBand(mark, band, method, begin, N, Fs, movAvgLen)
                        df = pandas.concat([df, pandas.DataFrame({"Band":band, "Method":method, "MovAvgLen":movAvgLen, "Mark":mark,
                                                                  "Energy":energies})])

        print "Plotting distributions"
        outFn = os.path.join(outDir, 'distributions.pdf')

        rdf = com.convert_to_r_dataframe(df.reset_index())
        rcols = robj.StrVector(COLORS[:len(marks)])
        gp = ggplot2.ggplot(rdf)
        
        pp = gp + \
            ggplot2.aes_string(x='factor(Band)', y='Energy') + \
            ggplot2.geom_boxplot(ggplot2.aes_string(fill='factor(Mark)', color='factor(Mark)'), outlier_size = 1) + \
            ggplot2.scale_colour_manual(values=rcols) + \
            ggplot2.scale_fill_manual(values=rcols) + \
            ggplot2.facet_grid(Formula("Method ~ MovAvgLen"), scales="free_y") + \
            ggplot2.theme(**{'axis.text': ggplot2.element_text(size=30),
                             'axis.title': ggplot2.element_text(size=30,face="bold"),
                             'axis.text.x': ggplot2.element_text(angle=90, hjust=1, vjust=1, size=30),
                             'legend.text': ggplot2.element_text(size=30),
                             'legend.title': ggplot2.element_text(size=30),
                             'strip.text': ggplot2.element_text(size=30)})
        ggsave(outFn, pp, dpi=300, width=24, height=18)


if __name__ == "__main__":
    markNameMap = {"m":"breathing meditation",
                   "c":"counting",
                   "r":"reading internet news",
                   "z":"reading zite",
                   "g":"playing a game",
                   "p":"programming",
                   "l":"led glasses",
                   "n":"noting meditation",
                   "e":"eyes open baseline",
                   "b":"eyes closed baseline"}

    bandFreqs = collections.OrderedDict([("02Hz delta",2), ("05Hz theta",5), ("08Hz lowAlpha",8), ("11Hz highAlpha",11), ("15Hz lowBeta",15), ("24Hz highBeta", 24), ("35Hz lowGamma", 35), ("45Hz highGamma", 45)])
            
    infn = sys.argv[1]
    outDir = os.path.basename(infn).split(".")[0]
    if not os.path.exists(outDir):
        os.makedirs(outDir)

    print "Loading EEG data"
    eegData = EEGData(infn, markNameMap, bandFreqs, minSize=SAMPLE_RATE*(START_SECS+1))

    print "Plotting Bands"
    eegData.plotBands(outDir, begin=int(SAMPLE_RATE*START_SECS), N=SAMPLE_RATE, Fs=SAMPLE_RATE,
                      movAvgLens=[0,10,30,75], methods=("ar", "multitaper"))

    print "Plotting PSDs"
    #eegData.plotPSDs(outDir, begin=int(SAMPLE_RATE*START_SECS), N=SAMPLE_RATE/3, Fs=SAMPLE_RATE,
    #                 methods=("ar", "multitaper", "periodogram", "fft"))

    print "Plotting distributions"
    eegData.plotDistributions(outDir, begin=int(SAMPLE_RATE*START_SECS), N=SAMPLE_RATE, Fs=SAMPLE_RATE,
                     movAvgLens=[0,10,30,75], methods=("ar", "multitaper"))

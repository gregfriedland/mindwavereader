import sys
import socket
import collections
import pygame
from time import sleep, time, strftime
import struct, array
import numpy
from config import OUTPUT_DATA_FILE, BLOCK_SIZE, VIZ_METHOD, \
    SAMPLE_RATE, BAND_FREQS, BAND_PLOT_RANGES
from EEGData import EEGData
from parseMindWave import parsePacket


ENABLE_BINARY_PACKET = False

fieldOrder = ["time", "rawEeg","attention","meditation","delta","theta","lowAlpha","highAlpha","lowBeta","highBeta","lowGamma","highGamma","mark"]

XINCR = 5
WIDTH = 1200
HEIGHT = 801

EMA_ALPHA = 0.3 # weight of contribution of new values to exponentionally weighted moving std (lower means slower decay of previous value)

def ema(prev, new, a):
  return a*prev + (1 - a) * new


def a_map(x, in_min, in_max, out_min, out_max):
  return float(x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min


def read_char():
    pause = True
    while pause:
        pause = False
        events = pygame.event.get()
        for event in events:
            if event.type == pygame.KEYDOWN:
                key = pygame.key.name(event.key)
                if key in "abcdefghijklmnopqrstuvwxyz1234567890[]\;',./-=`":
                    return key
                elif key == " ":
                    pause = True
                    sleep(0.01)
            elif event.type == pygame.QUIT:
                raise KeyboardInterrupt()

    return


def openConnection(host, port, binary=False):
    print "Connecting to host = " + host + ", port = " + str(port)
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect((host, port))

    print "Sending command"
    if binary:
        command = "{\"enableRawOutput\": true, \"format\": \"BinaryPacket\"}\n"
    else:
        command = "{\"enableRawOutput\": true, \"format\": \"Json\"}\n"
    sock.send(command)
    sock.setblocking(True)

    return sock


lastEegBlock = None
lastBandEnergies = numpy.zeros(len(BAND_FREQS))
lastMeanBandEnergies = numpy.zeros(len(BAND_FREQS))
x = XINCR
def draw(screen, signal, eegData, BLOCK_SIZE):
    global lastEegBlock, lastBandEnergies, lastMeanBandEnergies, x
    
    eegBlock = eegData.getLastEEGBlock(0, BLOCK_SIZE)
    #print "EegBlock: ", eegBlock, "EegData: ", len(eegData._vals)
    
    if eegBlock != lastEegBlock:
        if eegBlock is not None:
            bandEnergies = eegBlock.getBandEnergies(BAND_FREQS, VIZ_METHOD, SAMPLE_RATE)
            
            if bandEnergies is not None:
                for i, band in enumerate(bandEnergies):
                    minFreq, maxFreq = BAND_PLOT_RANGES.values()[i]
                
                    bandEnergies[i] = a_map(bandEnergies[i], minFreq, maxFreq, 0, 1)
                    #bandEnergies[i] = a_map(bandEnergies[i], 50, 80, 0, 1)

                meanBandEnergies = ema(lastMeanBandEnergies, bandEnergies, 1-EMA_ALPHA)
                
                setTone(440+bandEnergies[0]*100)
            
                panelHeight = HEIGHT / len(bandEnergies)
                for i, band in enumerate(bandEnergies):
                    minFreq, maxFreq = BAND_PLOT_RANGES.values()[i]
                    
                    currVal = bandEnergies[i]
                    lastVal = lastBandEnergies[i]
                    #currVal = a_map(bandEnergies[i], minFreq, maxFreq, 0, 1)
                    #lastVal = a_map(lastBandEnergies[i], minFreq, maxFreq, 0, 1)
                    pygame.draw.line(screen, (100,0,0), (x-XINCR,panelHeight*(i+1) - lastVal*panelHeight),
                                   (x,panelHeight*(i+1) - currVal*panelHeight))

                    #currMeanVal = a_map(meanBandEnergies[i], minFreq, maxFreq, 0, 1)
                    #lastMeanVal = a_map(lastMeanBandEnergies[i], minFreq, maxFreq, 0, 1)
                    currMeanVal = meanBandEnergies[i]
                    lastMeanVal = lastMeanBandEnergies[i]
                    pygame.draw.line(screen, (0,255,0), (x-XINCR,panelHeight*(i+1) - lastMeanVal*panelHeight),
                                   (x,panelHeight*(i+1) - currMeanVal*panelHeight))
                    
                    yline = panelHeight*(i+1)
                    pygame.draw.line(screen, (64,64,64), (0, yline), (WIDTH, yline))

                # advance the x marker
                x += XINCR
                if x >= WIDTH:
                    x = XINCR
                    screen.fill((0,0,0))
        
                lastMeanBandEnergies = meanBandEnergies
                    
            lastBandEnergies = bandEnergies
        lastEegBlock = eegBlock
    
    if signal is not None:
        if signal == 200: signalColor = (255,255,255)
        elif signal == 0: signalColor = (0,255,0)
        elif signal < 200: signalColor = (255,0,0)
        pygame.draw.ellipse(screen, signalColor, [WIDTH-50,0,50,50])


#from http://wearcam.org/ece516/mindset_communications_protocol.pdf
#def parseBinaryPacket(buf):
#    chars = buf
#    buf = [ord(c) for c in chars]
#    print "Buffer (%d): " % (len(buf)),
#    for c in buf: print "%02x" % c,
#    print
#
#    if len(buf) < 2:
#        return {}
#
#    while True:
#        # find the sync chars
#        i = 0
#        while True:
#            if buf[i] == 0xAA and buf[i+1] == 0xAA:
#                break
#            i += 1
#        i += 2
#
#        # parse the packet
#        data = {}
#        while i < len(buf):
#            if buf[i] == 0x02:
#                data["poorSignalLevel"] = buf[i+1]
#                i += 2
#            elif buf[i] == 0x04:
#                data["attention"] = buf[i+1]
#                i += 2
#            elif buf[i] == 0x05:
#                data["meditation"] = buf[i+1]
#                i += 2
#            elif buf[i] == 0x80:
#                data["rawEeg"] = struct.unpack("h", chars[i+1:i+3])[0]
#                i += 3
#            elif buf[i] == 0x81:
#                eegPowerLen = buf[i+1]
#                assert(eegPowerLen == 8*4)
#                i += 2
#                for eegPower in ["delta", "theta", "lowAlpha", "highAlpha", "lowBeta", "highBeta", "lowGamma", "highGamma"]:
#                    data[eegPower] = struct.unpack("f", chars[i:i+4])[0]
#                    i += 4
#            else:
#                raise Exception("Unknown type: 0x%x" % buf[i])
#
#        return data
#


def outputData(data, mark):
    vals = [str(data[field]) if field in data else "" for field in fieldOrder if field not in ("time", "mark")]
    if any(vals):
        print >> output, "%f,%s,%s" % (time()-startTime, ",".join(vals), mark)


def listenForData(sock, binary=False):
    sleep(0.01)
    buf = array.array('c', ' '*512)
    nbytes = sock.recv_into(buf)
    
    try:
        for data in parsePacket(buf):
            yield data
    except ValueError, e:
        print "Packet parse error:",
        if not binary:
            print buf.tostring(),
        print


AUDIO_SAMPLE_RATE = 44100
AUDIO_BUFFER_SIZE = AUDIO_SAMPLE_RATE

channel = None
def setTone(freq):
    global channel

    if channel is None:
        pygame.mixer.init(frequency=AUDIO_SAMPLE_RATE, size=-16, channels=1, buffer=AUDIO_BUFFER_SIZE)
        channel = pygame.mixer.Channel(0)
    
    if channel.get_queue():
        return
    
    adjFreq = int(freq * AUDIO_BUFFER_SIZE) / AUDIO_BUFFER_SIZE # so it ends at 0
    sndVals = 0.05 * (2**16-1) * numpy.sin(2*numpy.pi * adjFreq * numpy.arange(AUDIO_BUFFER_SIZE)/AUDIO_SAMPLE_RATE)
    sndVals = sndVals.astype(numpy.int16)
    #print sndVals.dtype, sndVals

    sound = pygame.mixer.Sound(buffer=sndVals)
    channel.queue(sound)


if __name__ == "__main__":
    startTime = time()
    pygame.init()
    screen = pygame.display.set_mode((WIDTH, HEIGHT))
    screen.fill((0,0,0))

    sock = openConnection("127.0.0.1", 13854, binary=ENABLE_BINARY_PACKET)

    output = open(OUTPUT_DATA_FILE, 'w')
    print >> output, ",".join(fieldOrder)

    setTone(440)
    
    mark = ""
    eegData = EEGData()
    try:
        while True:
            key = read_char()
            if key:
                mark = key

            for data in listenForData(sock, binary=ENABLE_BINARY_PACKET):
                outputData(data, mark)
                if "rawEeg" in data:
                    eegData.append(data["rawEeg"])
                signal = data.get("poorSignalLevel", None)

                draw(screen, signal, eegData, BLOCK_SIZE)
                    
            pygame.display.flip()
    except KeyboardInterrupt:
        import traceback
        traceback.print_exc()
    
    output.close()


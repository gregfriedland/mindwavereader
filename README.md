Read data from the NeuroSky mindwave using ThinkGear Connector in python or processing. Once the connection is established, eSense and EEG band values are written to an output file (at 1Hz).


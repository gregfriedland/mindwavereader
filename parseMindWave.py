import time
import sys
import json

SYNC_BYTE = 0xaa
EXCODE_BYTE = 0x55
POOR_SIGNAL_BYTE = 0x02
ATTENTION_BYTE = 0x04
MEDITATION_BYTE = 0x05
BLINK_STRENGTH_BYTE = 0x16
RAW_EEG_BYTE = 0x80
ASIC_EEG_BYTE = 0x83


rawEegTimeSent = time.time()

# adapted from https://www.npmjs.org/package/mindset-js-binary-parser
def parseBinaryPacket(buf):
    #var payLoadLength, packet, checkSum, checkSumExpected, parsedData, rawEeg, eegTick,
    #  payLoad, extendedCodeLevel, code, bytesParsed, dataLength, dataValue;

    nbuf = len(buf)
    while i < nbuf-3:
        payLoadLength = rawData[i+2]

        if buf[i] == SYNC_BYTE and buf[i+1] == SYNC_BYTE and payLoadLength < 170:
            packet = buf[i:i+payloadLength+4]
            checkSumExpected = packet[packet.length - 1]
            payLoad = packet[3:-1]
            #payLoad = payLoad.toJSON() # ???

            checkSum = 0
            checkSum = sum([p for p in payload])
            checkSum &= 0xFF
            checkSum = ~checkSum & 0xFF

            if checkSum == checkSumExpected:
                bytesParsed = 0
                parsedData = {}

                while bytesParsed < payLoadLength:
                    extendedCodeLevel = 0
                    while payLoad[bytesParsed] == EXCODE_BYTE:
                        extendedCodeLevel += 1
                        bytesParsed += 1

                    code = payLoad[bytesParsed]
                    bytesParsed += 1

                    dataLength = payLoad[bytesParsed] if (code & 0x80) else 1
                    bytesParsed += 1

                    if dataLength == 1:
                        dataValue = payLoad[bytesParsed]
                    else:
                        dataValue = [payload[bytesParsed+j] for j in range(dataLength)]
                    bytesParsed += dataLength

                    if extendedCodeLevel == 0:
                        if code == POOR_SIGNAL_BYTE:
                            parsedData["poorSignal"] = dataValue
                        elif code == ATTENTION_BYTE:
                            parsedData["attention"] = dataValue
                        elif code == MEDITATION_BYTE:
                            parsedData["meditation"] = dataValue
                        elif code == BLINK_STRENGTH_BYTE:
                            parsedData["blinkStrength"] = dataValue
                        elif code == RAW_EEG_BYTE:
                            eegTick = time.time()
                            #if (eegTick - rawEegTimeSent > 200){
                            rawEegTimeSent = eegTick
                            rawEeg = dataValue[0] * 256 + dataValue[1]
                            rawEeg = rawEeg - 65536 if rawEeg >= 32768 else rawEeg
                            parsedData["rawEeg"] = rawEeg
                        elif code == ASIC_EEG_BYTE:
                            parsedData["delta"] = dataValue[0] * 256 * 256 + dataValue[1] * 256 + dataValue[2]
                            parsedData["theta"] = dataValue[3] * 256 * 256 + dataValue[4] * 256 + dataValue[5]
                            parsedData["lowAlpha"] = dataValue[6] * 256 * 256 + dataValue[7] * 256 + dataValue[8]
                            parsedData["highAlpha"] = dataValue[9] * 256 * 256 + dataValue[10] * 256 + dataValue[11]
                            parsedData["lowBeta"] = dataValue[12] * 256 * 256 + dataValue[13] * 256 + dataValue[14]
                            parsedData["highBeta"] = dataValue[15] * 256 * 256 + dataValue[16] * 256 + dataValue[17]
                            parsedData["lowGamma"] = dataValue[18] * 256 * 256 + dataValue[19] * 256 + dataValue[20]
                            parsedData["highGamma"] = dataValue[21] * 256 * 256 + dataValue[22] * 256 + dataValue[23]

                yield parsedData
            else:
                print >> sys.stderr, "Invalid checksum"

            i = i + payLoadLength + 3
        else:
            i += 1


def parseJSONPacket(buf):
    for data in buf.tostring().strip().split("\r"):
        jsn = json.loads(data)
        yield flatten_dict(jsn)



def parsePacket(buf, binary=False):
    if binary:
        return parseBinaryPacket(buf)
    else:
        return parseJSONPacket(buf)


def flatten_dict(d):
    def items():
        for key, value in d.items():
            if isinstance(value, dict):
                for subkey, subvalue in flatten_dict(value).items():
                    #yield key + "." + subkey, subvalue
                    yield subkey, subvalue
            else:
                yield key, value

    return dict(items())




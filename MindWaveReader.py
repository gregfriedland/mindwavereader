import sys, json
import socket
import collections
import pygame
from time import sleep, time, strftime
import struct, array

ENABLE_RAW_OUTPUT = True

fieldOrder = ["time", "rawEeg","attention","meditation","delta","theta","lowAlpha","highAlpha","lowBeta","highBeta","lowGamma","highGamma","mark"]

def flatten_dict(d):
    def items():
        for key, value in d.items():
            if isinstance(value, dict):
                for subkey, subvalue in flatten_dict(value).items():
                    #yield key + "." + subkey, subvalue
                    yield subkey, subvalue
            else:
                yield key, value

    return dict(items())


def read_char():
    events = pygame.event.get()
    for event in events:
        if event.type == pygame.KEYDOWN:
            key = pygame.key.name(event.key)

            # if key == 'c' and (pygame.key.get_mods() & pygame.KMOD_CTRL):
            #     raise KeyboardInterrupt()
            if key in "abcdefghijklmnopqrstuvwxyz1234567890[]\;',./-=`":
                return key

        elif event.type == pygame.QUIT:
            raise KeyboardInterrupt()

    return None


def openConnection(host, port):
    print "Connecting to host = " + host + ", port = " + str(port)
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect((host, port))

    print "Sending command"
    if ENABLE_RAW_OUTPUT:
        command = "{\"enableRawOutput\": true, \"format\": \"Json\"}\n"
    else:
        command = "{\"enableRawOutput\": false, \"format\": \"Json\"}\n"
    sock.send(command)
    sock.setblocking(True)

    return sock


def draw(signal):
    if signal is not None:
        if signal == 200: signalColor = (255,255,255)
        elif signal == 0: signalColor = (0,255,0)
        elif signal < 200: signalColor = (255,0,0)
        pygame.draw.ellipse(screen, signalColor, [50,50,100,100])

#    if pygame.font:
#        font = pygame.font.Font(None, 24)
#        text = font.render("%.0f" % (time() - startTime), 1, (10, 10, 10))
#        textpos = text.get_rect(centerx=background.get_width()/2)
#        background.blit(text, textpos)    


def parseJSONPacket(buf):
    try:
        #print "Received:", buf
        jsn = json.loads(buf)
        return flatten_dict(jsn)
    except ValueError, e:
        print "Error decoding JSON packet: %s" % e
        return {}


def parseBinaryPacket(buf, processDataFunc):
    chars = buf
    buf = [ord(c) for c in chars]
    print "Buffer (%d): " % (len(buf)),
    for c in buf: print "%02x" % c,
    print

    if len(buf) < 2:
        return {}

    while True:
        # find the synch chars
        i = 0
        while True:
            if buf[i] == 0xAA and buf[i+1] == 0xAA:
                break
            i += 1
        i += 2

        # parse the packet
        data = {}
        while i < len(buf):
            if buf[i] == 0x02:
                data["poorSignalLevel"] = buf[i+1]
                i += 2
            elif buf[i] == 0x04:
                data["attention"] = buf[i+1]
                i += 2
            elif buf[i] == 0x05:
                data["meditation"] = buf[i+1]
                i += 2
            elif buf[i] == 0x80:
                data["rawEeg"] = struct.unpack("h", chars[i+1:i+3])[0]
                i += 3
            elif buf[i] == 0x81:
                eegPowerLen = buf[i+1]
                assert(eegPowerLen == 8*4)
                i += 2
                for eegPower in ["delta", "theta", "lowAlpha", "highAlpha", "lowBeta", "highBeta", "lowGamma", "highGamma"]:
                    data[eegPower] = struct.unpack("f", chars[i:i+4])[0]
                    i += 4
            else:
                processDataFunc(data)
                break


mark = ""
def processData(data):
    global mark

    if "poorSignalLevel" in data:
       signal = data["poorSignalLevel"]
    else:
        signal = None
    
    key = read_char()
    if key:
        mark = key

    vals = [str(data[field]) if field in data else "" for field in fieldOrder if field not in ("time", "mark")]
    if any(vals):
        print >> output, "%f,%s,%s" % (time()-startTime, ",".join(vals), mark)

    return signal


if __name__ == "__main__":
    startTime = time()
    pygame.init()
    screen = pygame.display.set_mode((200,200))
    screen.fill((0,0,0))

    sock = openConnection("127.0.0.1", 13854)

    output = open("mindwave.out", 'w')
    print >> output, ",".join(fieldOrder)

    try:
        while True:
            buf = array.array('c', ' '*512)
            nbytes = sock.recv_into(buf)
            buf = buf.tostring().strip()

            if buf == "": continue
            
            for data in buf.split("\r"):
                if ENABLE_RAW_OUTPUT:
                    #data = parseBinaryPackets(data, processData)
                    data = parseJSONPacket(data)
                else:
                    data = parseJSONPacket(data)
                
                #print "Data: ", data
                signal = processData(data)

                if signal is not None:
                    draw(signal)
                    
            pygame.display.flip()
    except KeyboardInterrupt:
        pass
    
    output.close()

